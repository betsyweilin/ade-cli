.. _create-custom-volume:

Creating a custom volume
^^^^^^^^^^^^^^^^^^^^^^^^

ADE volumes are a way to minimize the size of the base image by loading
different resources from a different Docker image. ADE volumes
can be used to provide third-party libraries, IDEs, and proprietary programs.

Let's look at how a volume for `Atom`_ can be created [#f1]_:

1. A generic Docker file:

   .. literalinclude:: ade-atom/Dockerfile
      :language: docker

   * Note the ``CMD``, which is meant to keep the container running even if no one
     is attached

2. A script to create ``/opt/atom``:

   .. literalinclude:: ade-atom/build-opt
      :language: bash

   * Usage: ``./build-opt 1.35.0``
   * This is where the bulk of the work happens:
      * Using the version number provided, the script downloads Atom and installs
        it into a directory that will become ``/opt/atom`` in the Dockerfile:

      .. literalinclude:: ade-atom/Dockerfile
         :language: docker
         :lines: 2

3. A script to set up the environment on ``ade enter``:

   .. literalinclude:: ade-atom/env.sh
      :language: bash

   * At a minimum, ``env.sh`` needs to add the executable to ``PATH``
   * In this case it sets the location where Atom will store it's configuration
     such that they will persiste between restarts of ADE
   * Note that ``build-opt`` takes care of copying this file into ``/opt/atom``
     and naming it ``.env.sh``:

     .. literalinclude:: ade-atom/build-opt
        :language: bash
        :lines: 23


4. A script of commands that should be run during ``ade start``:

   .. literalinclude:: ade-atom/adeinit
      :language: bash

   * Note that ``build-opt`` takes care of copying this file into ``/opt/atom``
     and naming it ``.adeinit``:

     .. literalinclude:: ade-atom/build-opt
        :language: bash
        :lines: 24

Placing these four files in a `Gitlab project`_ and adding a `CI script`_ to automate
the ``docker build`` command, makes it simple to generate new images on demand by creating
a ``git`` tag for the desired version

For a more complex example, where the software is developed in the same repository, see
the `AutowareAuto project`_. AutowareAuto generates ADE volumes which can be used
to deploy a pre-built AutowareAuto build in another machine.

.. [#f1] The code for this example is kept in this `Gitlab project`_

.. _Atom: https://atom.io
.. _Gitlab project: https://gitlab.com/ApexAI/ade-atom
.. _CI script: https://gitlab.com/ApexAI/ade-atom/blob/master/.gitlab-ci.yml
.. _AutowareAuto project: https://gitlab.com/AutowareAuto/AutowareAuto

